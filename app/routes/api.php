<?php
use Illuminate\Http\JsonResponse as Response;
use Illuminate\Http\Request;

/**
 * @var $router Laravel\Lumen\Routing\Router
 */
$router->get('/ping', function (Response $response) {
    echo "PING";
});

$router->get('/users[/{id}]', 'UserController@get');

