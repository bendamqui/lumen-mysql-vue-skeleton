<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Database\Eloquent\Collection;

abstract class BaseRepository
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return Collection|static[]
     */
    public function getAll($columns = ['*'])
    {
        return $this->model->all($columns);
    }

    /**
     * @param $id
     * @return Model
     */
    public function getOne($id)
    {
        return $this->model->find($id);
    }

    /**
     * @param array $payload
     * @return Model
     */
    public function create($payload)
    {
        $this->model->fill($payload)->save();
        return $this->model;
    }

    /**
     * @param int $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteOne($id)
    {
        return $this->getOne($id)->delete();
    }
}
