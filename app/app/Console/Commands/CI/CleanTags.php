<?php

namespace App\Console\Commands\CI;

use Illuminate\Console\Command;
use Bendamqui\Gitlab\Api\ContainerRegistry;
use Exception;

class CleanTags extends Command
{
    /**
     * @var string
     */
    protected $signature = 'ci:clean-tags {project_id} {tag} {token}';

    /**
     * @var string
     */
    protected $description = 'Clean up gitlab container registry';

    /**
     * @var ContainerRegistry
     */
    private $api;

    public function handle()
    {
        $project_id = $this->argument('project_id');
        $tag = $this->argument('tag');
        $token = $this->argument('token');
        $this->api = ContainerRegistry::factory($token);

        $this->info(sprintf(
            'Clean up tag %s for container registry of project %s.',
            $tag,
            $project_id
            )
        );

        $this->clean($project_id, $tag);

    }

    /**
     * @param int $project_id
     * @param string $tag
     * @throws Exception
     */
    private function clean($project_id, $tag)
    {
        foreach ($this->getRepositories($project_id) as $repository) {
            $this->deleteTag($project_id, $repository['id'], $tag);
        }
    }

    /**
     * @param int $project_id
     * @param int $repository
     * @throws Exception
     * @return array
     */
    private function getTags($project_id, $repository)
    {
        return $this->api
            ->project($project_id)
            ->repository($repository)
            ->getTags();
    }

    /**
     * @param int $project_id
     * @param int $repository
     * @param string $tag
     * @throws Exception
     * @return bool
     */
    private function tagExists($project_id, $repository, $tag)
    {
        return in_array($tag, array_column($this->getTags($project_id, $repository), 'name'));
    }

    /**
     * @param int $project_id
     * @param int $repository
     * @param string $tag
     * @throws Exception
     */
    private function deleteTag($project_id, $repository, $tag)
    {
        if ($this->tagExists($project_id, $repository, $tag)) {
            $this->api
                ->project($project_id)
                ->repository($repository)
                ->deleteTag($tag);
        }
    }

    /**
     * @param int $project_id
     * @throws Exception
     * @return array
     */
    private function getRepositories($project_id)
    {
        return $this->api
            ->project($project_id)
            ->getRepositories();
    }
}
