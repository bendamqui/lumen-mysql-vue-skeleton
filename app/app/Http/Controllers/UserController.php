<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $user;

    /**
     * UserController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @param Response $response
     * @param null $id
     * @return Response
     */
    public function get(JsonResponse $response, $id = null)
    {
        $data = is_null($id) ? $this->user->getAll() : $this->user->getOne($id);
        return $response->setData($data);
    }
}
