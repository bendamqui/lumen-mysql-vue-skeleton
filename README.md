## Description

Dockerized Lumen, Nginx, Mysql, Vue.js skeleton with gitlab ci and Vue.js hot reload for development environment

## Getting started

### Dev

Generate a self signed certificate.

`./bin/gen-cert`

Set the environment variables.

`cp .env.example .env`

Spin up services.

`docker-compose up --build`

Run migration once composer install is completed.

`docker-compose exec php php artisan migrate --seed`






