#!/bin/bash
if ! cmp -s /usr/src/app/package-lock.json /package-version/package-lock.json; then
    echo 'Run npm install...'
    if npm install; then
    	cp /usr/src/app/package-lock.json /package-version/package-lock.json
    fi
fi
exec "$@"
