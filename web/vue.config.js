const fs = require('fs-extra');

const config = {
    lintOnSave: false
};

if (process.env.NODE_ENV === 'development') {
    config.devServer = {
        disableHostCheck: true,
        https: {
            key: fs.readFileSync('/ssl/private.key'),
            cert: fs.readFileSync('/ssl/private.crt')
        },
        proxy: {
            '^/api' : {
                target: 'https://server:443',
                changeOrigin: true
            }

        }
    }
}

module.exports = config;
